-----------------------
-- Подготовительные данные
-----------------------
create table timework (
    ID int,
    EmployeeID int,
    StartPeriod timestamp,
    EndPeriod timestamp
)

insert into timework (ID, EmployeeID, StartPeriod, EndPeriod)
values (1, 1, '2021-08-11 12:00:00', '2021-08-11 18:00:00'),
       (2, 1, '2021-08-12 12:00:00', '2021-08-12 18:00:00'),
       (3, 1, '2021-08-16 12:00:00', '2021-08-16 18:00:00'),
       (4, 2, '2021-08-11 12:00:00', '2021-08-11 18:00:00'),
       (5, 2, '2021-08-14 12:00:00', '2021-08-14 18:00:00');

CREATE EXTENSION IF NOT EXISTS tablefunc;

-----------------------
-- Следующие запросы решают поставленную задачу
-----------------------
CREATE VIEW tw_view AS
SELECT
    EmployeeID,
    EndPeriod - StartPeriod AS interval,
    DATE_TRUNC('day', StartPeriod) AS day
FROM timework;

SELECT * FROM CROSSTAB ($$
    SELECT
        EmployeeID,
        day,
        interval
    FROM tw_view ORDER BY 1
  $$,$$
    SELECT GENERATE_SERIES (
      TIMESTAMP '2021-08-11',  -- start day
      TIMESTAMP '2021-08-16',  -- end day
      '1 day'
    )
  $$
) AS (
    Employees TEXT
    , "11.08" TEXT
    , "12.08" TEXT
    , "13.08" TEXT
    , "14.08" TEXT
    , "15.08" TEXT
    , "16.08" TEXT
);

DROP view IF EXISTS tw_view